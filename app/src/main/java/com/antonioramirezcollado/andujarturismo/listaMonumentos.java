package com.antonioramirezcollado.andujarturismo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.os.AsyncTask;
import android.util.Log;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class listaMonumentos extends AppCompatActivity {

    public final static String NUM_MONUMENTO = "com.antonioramirezcollado.andujarturismo.NUM_BOTON";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    // TODO: Indicar la URL donde se encuentre el documento XML
    private static final String URL = "http://andujarturismo.esy.es/andujarTurismo.xml";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_monumentos);

        /*
         * Si queromos cojer los datos de los objetos creados en la aplicación.
        if(Monumentos.monumentoList.isEmpty()){
            Monumentos.loadSimpleSamples();
        }
        */
        // Si queremos cojer los datos del XML
        new DownloadXmlTask().execute(URL);

        mRecyclerView = (RecyclerView) findViewById(R.id.recargarLista);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new adaptarDatosListaRecycler(this.getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Monumento monumento = Monumentos.monumentoList.get(position);
                //Esto solo es para mostrar un mensaje con el objeto selecionado de la lista.
                //Toast.makeText(getApplicationContext(), monumento.getId() + " is selected!", Toast.LENGTH_LONG).show();
                Intent intentSegundaActivity = new Intent(getApplicationContext(), andujar.class);
                intentSegundaActivity.putExtra(NUM_MONUMENTO, position);
                startActivity(intentSegundaActivity);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    private class DownloadXmlTask extends AsyncTask<String, Void, Integer> {
        @Override
        protected Integer doInBackground(String... urls) {
            try {
                InputStream stream = null;
                try {
                    stream = downloadUrl(urls[0]);
                    XMLParser myXmlParser = new XMLParser();
                    Monumentos.monumentoList = myXmlParser.parse(stream);
                } finally {
                    if (stream != null) {
                        stream.close();
                    }
                }
                // Descarga correcta
                return 0;
            } catch (IOException e) {
                // Error de conexión
                return 1;
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                // Error en los datos
                return 2;
            }
        }

        @Override
        protected void onPostExecute(Integer resultCode) {
            switch(resultCode) {
                case 0:
                    // TODO: Realizar aquí el tratamiento oportuno de la lista de objetos que se
                    //  ha descargado
                    mRecyclerView.setAdapter(mAdapter);
                    break;
                case 1:
                    // TODO: Mostrar los mensajes de error en el lugar oportuno
                    Log.w(this.getClass().getName(), "Error de conexión");
                    break;
                case 2:
                    Log.w(this.getClass().getName(), "Error en los datos");
                    break;
            }
        }
    }

    // Given a string representation of a URL, sets up a connection and gets
    // an input stream.
    private InputStream downloadUrl(String urlString) throws IOException {
        //(No hace el import automáticamente ¿?) import java.net.URL;
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        conn.connect();
        return conn.getInputStream();
    }

}