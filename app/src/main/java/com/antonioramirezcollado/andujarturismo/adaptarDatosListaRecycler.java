package com.antonioramirezcollado.andujarturismo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class adaptarDatosListaRecycler extends RecyclerView.Adapter<adaptarDatosListaRecycler.ViewHolder> {
    // Cambiar por una referencia a la lista real que contenga los datos
    private Context context;

    public adaptarDatosListaRecycler(Context context){
        this.context = context;
    }

    // Crear una referencia a los elementos View (TextView, ImageView, etc)
    // por cada dato que se vaya a mostrar.
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView1;
        public TextView mTextView2;
        public ImageView mImageView1;

        public ViewHolder(View v) {
            super(v);
            mTextView1 = (TextView) v.findViewById(R.id.textView1);
            mTextView2 = (TextView) v.findViewById(R.id.textView2);
            mImageView1 = (ImageView) v.findViewById(R.id.imageView1);
        }
    }

    @Override
    public adaptarDatosListaRecycler.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        // En la siguiente línea se debe indicar el nombre que se le ha asignado al layout anterior
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_elementos_lista, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Asociar cada elemento View con el dato a mostrar
        holder.mTextView1.setText(Monumentos.monumentoList.get(position).getTitulo());
        holder.mTextView2.setText(Monumentos.monumentoList.get(position).getDireccion());

        int id = context.getResources().getIdentifier("drawable/" + Monumentos.monumentoList.get(position).
                getImagenMonumento(), "drawable", context.getPackageName());
        holder.mImageView1.setImageResource(id);
    }

    // Retorna el número de elementos de la lista
    @Override
    public int getItemCount() {
        return Monumentos.monumentoList.size();
    }

}