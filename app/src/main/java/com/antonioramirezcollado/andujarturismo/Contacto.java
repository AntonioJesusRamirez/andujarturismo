package com.antonioramirezcollado.andujarturismo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Contacto extends Fragment {

    private TextView direccion;
    private TextView direccionWeb;
    private TextView telefono;
    private TextView correo;


    public Contacto() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_contacto, container, false);

        direccion = (TextView) v.findViewById(R.id.tV_dir);
        direccion.setText(andujar.monumento.getDireccion());

        direccionWeb = (TextView) v.findViewById(R.id.tV_web);
        direccionWeb.setText(andujar.monumento.getWeb());

        telefono = (TextView) v.findViewById(R.id.tV_tel);
        telefono.setText(andujar.monumento.getTelefono());

        correo = (TextView) v.findViewById(R.id.tV_con);
        correo.setText(andujar.monumento.getCorreo());

        return v;
    }


}
