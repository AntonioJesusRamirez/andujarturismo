package com.antonioramirezcollado.andujarturismo;

import android.widget.ImageView;

/**
 * Created by 2DAW3 on 17/11/2016.
 */
//Esta clase la aprovecho para decir como va a ser dicho objeto y para el documento XML:
public class Monumento {

    private String titulo;
    private String imagenMonumento;
    private String descripcion;
    private String direccion;
    private String web;
    private String telefono;
    private String correo;
    private Double coordenadaX;
    private Double coordenadaY;
    private Integer id;

    public Monumento(){

    }

    public Monumento(String titulo, String imagenMonumento, String descripcion, String direccion, String web, String telefono, String correo, Double coordenadaX, Double coordenadaY, Integer id) {
        this.titulo = titulo;
        this.imagenMonumento = imagenMonumento;
        this.descripcion = descripcion;
        this.direccion = direccion;
        this.web = web;
        this.telefono = telefono;
        this.correo = correo;
        this.coordenadaX = coordenadaX;
        this.coordenadaY = coordenadaY;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCoordenadaX() {
        return coordenadaX;
    }

    public void setCoordenadaX(Double coordenadaX) {
        this.coordenadaX = coordenadaX;
    }

    public Double getCoordenadaY() {
        return coordenadaY;
    }

    public void setCoordenadaY(Double coordenadaY) {
        this.coordenadaY = coordenadaY;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getImagenMonumento() {
        return imagenMonumento;
    }

    public void setImagenMonumento(String imagenMonumento) {
        this.imagenMonumento = imagenMonumento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    //Esto es para el XML
    @Override
    public String toString() {
        return "Monumento{" +
                "titulo=" + titulo +
                ", imagenMonumento='" + imagenMonumento + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", direccion='" + direccion + '\'' +
                ", web='" + web + '\'' +
                ", telefono='" + telefono + '\'' +
                ", correo='" + correo + '\'' +
                ", coordenadaX='" + coordenadaX + '\'' +
                ", coordenadaY='" + coordenadaY + '\'' +
                ", id=" + id +
                '}';
    }

}
