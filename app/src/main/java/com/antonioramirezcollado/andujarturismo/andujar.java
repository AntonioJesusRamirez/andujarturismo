package com.antonioramirezcollado.andujarturismo;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import java.util.List;

public class andujar extends AppCompatActivity {

    public static Monumento monumento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_andujar);

        MyPagerAdapter myPagerAdapter =
                new MyPagerAdapter(getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.visorDePaginas);
        viewPager.setAdapter(myPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        Intent intent = getIntent();
        int numeroMonumento = intent.getIntExtra(listaMonumentos.NUM_MONUMENTO, 0);
        monumento = Monumentos.monumentoList.get(numeroMonumento);
    }

    // Dirección web
    // Preguntar con que saplicación se desea abrir y si hay alguna.
    public void web (View view){
        Uri web = Uri.parse(andujar.monumento.getWeb());
        Intent webIntent = new Intent(Intent.ACTION_VIEW, web);

        PackageManager packageManager = this.getPackageManager();

        List<ResolveInfo> activities = packageManager.queryIntentActivities(webIntent, 0);
        if(activities.size() > 0) {
            startActivity(Intent.createChooser(webIntent, "Abrir con ..."));
        } else {
            Toast aviso = Toast.makeText(this, "No hay ninguna aplicación disponible", Toast.LENGTH_SHORT);
            aviso.show();
        }
    }

    // Llamar por teléfono.
    // Preguntar con que saplicación se desea abrir y si hay alguna.
    public void telefono (View view) {
        Uri telefono = Uri.parse("tel:" + andujar.monumento.getTelefono());
        Intent telefonoIntent = new Intent(Intent.ACTION_DIAL, telefono);

        PackageManager packageManager = this.getPackageManager();

        List<ResolveInfo> activities = packageManager.queryIntentActivities(telefonoIntent, 0);
        if(activities.size() > 0) {
            startActivity(telefonoIntent);
        } else {
            Toast aviso = Toast.makeText(this, "No hay ninguna aplicación disponible", Toast.LENGTH_SHORT);
            aviso.show();
        }
    }

    // Enviar correo electrónico
    // Preguntar con que saplicación se desea abrir y si hay alguna.
    public void correo (View view){
        Intent correo = new Intent(Intent.ACTION_SEND);
        // El intent para envío (ACTION_SEND) no usa el parámetro URI, por lo que se declara el tipo "text/plain"
        correo.setType("text/plain");
        correo.putExtra(Intent.EXTRA_EMAIL, new String[] {andujar.monumento.getCorreo()});

        PackageManager packageManager = this.getPackageManager();

        List<ResolveInfo> activities = packageManager.queryIntentActivities(correo, 0);
        if(activities.size() > 0) {
            startActivity(Intent.createChooser(correo, "Enviar email con ..."));
        } else {
            Toast aviso = Toast.makeText(this, "No hay ninguna aplicación disponible", Toast.LENGTH_SHORT);
            aviso.show();
        }
    }



    // Funcionalidad de la flecha para volver atras
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        // el int lo coge por defecto y es el numero de la pagina que se esta visualizando.
        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = new DescripcionGeneral();
                    break;
                case 1:
                    fragment = new ComoLlegar();
                    break;
                case 2:
                    fragment = new Contacto();
                    break;
                default:
                    fragment = null;
            }
            return fragment;
        }

        // cuenta las paginas que hay.
        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    String textoDescripcion = getResources().getString(R.string.descripcion);
                    return textoDescripcion;
                case 1:
                    String textoComoLlegar = getResources().getString(R.string.comoLlegar);
                    return textoComoLlegar;
                case 2:
                    String textoContactar = getResources().getString(R.string.contactar);
                    return textoContactar;
            }
            return null;
        }
    }
}

