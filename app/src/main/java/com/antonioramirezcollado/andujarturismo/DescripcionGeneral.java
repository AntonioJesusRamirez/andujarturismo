package com.antonioramirezcollado.andujarturismo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DescripcionGeneral extends Fragment {

    private TextView titulo;
    private TextView descripcion;
    private ImageView imagen;

    public DescripcionGeneral() {
        // Required empty public constructor
    }

    /*
     * Recoge los datos del monumento y los introduce en los campos correspondientes.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_descripcion_general, container, false);

        titulo = (TextView) v.findViewById(R.id.tV_Titulo);
        titulo.setText(andujar.monumento.getTitulo());

        descripcion = (TextView) v.findViewById(R.id.tV_Descripcion);
        descripcion.setText(andujar.monumento.getDescripcion());

        imagen = (ImageView) v.findViewById(R.id.iV_Monumento);
        int id = getResources().getIdentifier(
                "drawable/" + andujar.monumento.getImagenMonumento(), "drawable", this.getContext().getPackageName());
        imagen.setImageResource(id);

        return v;
    }

}